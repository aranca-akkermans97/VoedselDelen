# Voedsel Delen - Social Machine Learning Experiment

I started this experiment to discover how machine learning works and how I can make them learn things I want them to. To start with I received the below documented sandbox that I modified to suite my desired experiment.

I made the original agents react to each other and made them learn from hugging each other as well as from hunger. The end goal was te make them share their food so that they didn't suffer from hunger.

## Demo

[Voedsel Delen online demo](https://arancaakkermans.com/voedseldelen/)

## Received code description

### Sandbox for WaterWorld using REINFORCEjs

This project serves as a simple starting point and sandbox to experiment with different types of WaterWorld games and 'social' experiments.

It is based on the Waterworld demo that is included with REINFORCEjs.

**REINFORCEjs** is a Reinforcement Learning library that implements several common RL algorithms, all with web demos. In particular, the library currently includes:

See the [main webpage](http://cs.stanford.edu/people/karpathy/reinforcejs) for many more details, documentation and demos.

### License

MIT.
